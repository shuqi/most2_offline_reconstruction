# CMakeLists.txt for event package. It creates a library with dictionary and a main program
cmake_minimum_required(VERSION 3.0 FATAL_ERROR)
project(MOST2_Offline_Reconstruction)

# You need to tell CMake where to find the ROOT installation. This can be done in a number of ways:
#   - ROOT built with classic configure/make use the provided $ROOTSYS/etc/cmake/FindROOT.cmake
#   - ROOT built with CMake. Add in CMAKE_PREFIX_PATH the installation prefix for ROOT
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})

#---Locate the ROOT package and defines a number of variables (e.g. ROOT_INCLUDE_DIRS)
find_package(ROOT REQUIRED COMPONENTS MathCore RIO Hist Tree Net)

#---Define useful ROOT functions and macros (e.g. ROOT_GENERATE_DICTIONARY)
include(${ROOT_USE_FILE})

include_directories(${CMAKE_SOURCE_DIR}/include ${ROOT_INCLUDE_DIRS})
add_definitions(${ROOT_CXX_FLAGS})

#ROOT_GENERATE_DICTIONARY(G__DeBinary.h LINKDEF EventLinkDef.h)

set(CMAKE_CXX_FLAGS ${ROOT_CXX_FLAGS})
set(LIBRARY_OUTPUT_PATH ${CMAKE_CURRENT_SOURCE_DIR}/lib)
aux_source_directory(${CMAKE_CURRENT_SOURCE_DIR}/src LIBSRC)

#---Create a shared library with geneated dictionary
add_library(Models SHARED ${LIBSRC})
target_link_libraries(Models PUBLIC ${ROOT_LIBRARIES})

#---Create  a main program using the library
aux_source_directory(${CMAKE_CURRENT_SOURCE_DIR}/util MAINSRC)
add_executable(runReco ${MAINSRC})
target_link_libraries(runReco Models ${ROOT_LIBRARIES})
