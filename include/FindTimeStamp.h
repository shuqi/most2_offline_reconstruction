#ifndef _FindTimeStamp_H
#define _FindTimeStamp_H 1

#include <iostream>
#include <fstream>
#include <vector>

#include <TString.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include "constants.h"

class FindTimeStamp{

protected:
  
    TFile *file1;    // Chip01
    TFile *file2;    // Chip02
    TFile *file3;    // Chip03
    TFile *file4;    // Chip04
    TFile *file5;    // Chip05
    TFile *file6;    // Chip06
    
    int maxLoop;
    int minLoop;
    int maxPosLoop;
    int minPosLoop;

public:

  FindTimeStamp();
  ~FindTimeStamp();
  
  bool initialize(TString directory, TString RunNumber);
  std::vector<std::vector<int>> FindTurnPoint();
  void FindTime(std::vector<std::vector<int>> vec_Findpos, TString directory, TString RunNumber);

private:
};

std::vector<int> FindZero(TFile *inputFile)
{
  std::vector<int> vec_Findpos;
  TTree *t = (TTree*)inputFile->Get("HitsInfo");
  int timeFPGA;
  t->SetBranchAddress("timeFPGA", &timeFPGA);
  int pretimeFPGA = 0;
  int curtimeFPGA = 0;
  vec_Findpos.clear();
  for (int i = 0; i < (int)t->GetEntries(); i++)
  {
    t->GetEntry(i);
    pretimeFPGA = curtimeFPGA;
    curtimeFPGA = timeFPGA;
    if (pretimeFPGA > curtimeFPGA)
    {     
      // if (std::abs(pretimeFPGA - curtimeFPGA < 200000000)) continue;
      std::cout << "pretimeFPGA > curtimeFPGA" << std::endl;
      std::cout << "position = " << i << std::endl;
      std::cout << "pretimeFPGA = " << pretimeFPGA << std::endl;
      std::cout << "curtimeFPGA = " << curtimeFPGA << std::endl;
      std::cout << "====================" << std::endl;
      vec_Findpos.push_back(i);
    }
  }
  return vec_Findpos;
}


#endif
