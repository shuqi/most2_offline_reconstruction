#ifndef _CreateCluster_H
#define _CreateCluster_H 1

#include <iostream>
#include <fstream>
#include <vector>

#include <TString.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include "IncludeFun.h"
#include "constants.h"

class CreateCluster{

protected:
  
public:

  CreateCluster();
  ~CreateCluster();
  
  void FormCluster(TString directory, TString RunNumber, TString hitOption);

private:

};


int equals(int a, int b)
{
  return a == b;
}

void FindSamePosXY(std::vector<int> &src, std::map<int, int> &dst)
{
  for (int i = 0; i < src.size(); ++i)
  {
    int checkStr = src[i];
    int flag = 0;
    for (int j = 0; j < i; ++j)
    {
      if (equals(checkStr, src[j]))
      {
        flag = 1;
        break;
      }
    }
    if (!flag)
    {
      int count = 1;
      for (int j = i + 1; j < src.size(); ++j)
      {
        if (equals(checkStr, src[j]))
        {
          ++count; 
        }
      }
      dst.insert(std::pair<int, int>(checkStr, count));
    
    }
  
  }
 
}

void pixelToPoint(double row, double col, double Point[2])
{

  if (col <= 512) // 451 // 512 1023/2
  {
    Point[0] = -((512 - col)*25);  //um
  }
  else
  {
    Point[0] = (col - 512)*25;
  }

  if (row  <= 256)  // 238 // 256
  {
    Point[1] = -((256 - row)*25);
  }
  else
  {
    Point[1] = (row - 256)*25;
  }
}

#endif
