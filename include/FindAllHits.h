#ifndef _FindAllHits_H
#define _FindAllHits_H 1

#include <iostream>
#include <fstream>
#include <vector>

#include <TString.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include "IncludeFun.h"
#include "constants.h"

class FindAllHits{

protected:
  
public:

  FindAllHits();
  ~FindAllHits();
  
  void FindHits(std::vector<std::vector<int>> vec_Findpos, TString directory, TString RunNumber);

private:
};

int GetCorrectCol(int row, int col)
{
  int newCol;
  if ((row%4 == 0) || (row%4 == 3)){ newCol = col*2; }
  else { newCol = col*2+1; }
  return newCol;
}

template<typename T>
std::vector<int> findItems(std::vector<T> const &v, int target) {
    std::vector<int> indices;
    auto it = v.begin();
    while ((it = std::find_if(it, v.end(), [&] (T const &e) { return e == target; }))
        != v.end())
    {
        indices.push_back(std::distance(v.begin(), it));
        it++;
    }
    return indices;
}

std::vector<int> FindTimeChip(std::vector<int> vec_timeChipOut)
{
  int j = 0;
  std::vector<int> pos;
  int goal;
  std::vector<int> vec_timeChipIDOut;
  vec_timeChipIDOut.clear();
  for (int i = 0; i < vec_timeChipOut.size(); i++)
  {
     vec_timeChipIDOut.push_back(-1);
  }
  // pos.clear();
  for (int i = 0; i < vec_timeChipOut.size(); i++)
  {
     //std::cout << "i = " << i << std::endl;
     if (i != 0 && vec_timeChipIDOut.at(i) != -1) continue;
     goal = vec_timeChipOut.at(i);
     // std::cout << "goal = " << goal << std::endl;
     for (int kk = 0; kk < 6; kk++)
     {
       std::vector<int> pos_kk;
       pos_kk = findItems(vec_timeChipOut, goal+kk);
       pos.insert(pos.end(), pos_kk.begin(), pos_kk.end());
       pos_kk.clear();
     }
     
     // std::cout << "indices.size() = " << pos.size() << std::endl;
     
     for (int m = 0; m < pos.size(); m++)
     {    
       // std::cout << "vec_timeChipOut.at(pos.at(m)) = " << vec_timeChipOut.at(pos.at(m)) << std::endl;
       vec_timeChipIDOut.at(pos.at(m)) = j; 
     }
     // std::cout << "j = " << j << std::endl;
     j++;
     pos.clear();
  }
  // std::cout << "vec_timeChipIDOut.size() = " << vec_timeChipIDOut.size() << std::endl;
  return vec_timeChipIDOut;
}

int FindTime(TFile *inputFile, int target, std::vector<std::vector<int>*> outputTree, int chipID, int tag[6]){
  TTree *t = (TTree*)inputFile->Get("HitsInfo");
  int row, col, timeFPGA, timeChip;
  t->SetBranchAddress("row", &row);
  t->SetBranchAddress("col", &col);
  t->SetBranchAddress("timeFPGA", &timeFPGA);
  t->SetBranchAddress("timeChip", &timeChip);
  // std::cout << (int)t->GetEntries() << std::endl;
  // std::cout << "chipID = " << chipID << std::endl; std::cout << "tag = " << tag[chipID] << std::endl; std::cout << "=============" << std::endl;
  int FindHitsNum = 0;
  for (int i = tag[chipID]; i < (int)t->GetEntries(); i++)
  {
    t->GetEntry(i);
    if (passTime(target, timeFPGA, 1))
    {
      tag[chipID] = i;
      outputTree[0]->push_back(chipID);
      outputTree[1]->push_back(timeFPGA);
      outputTree[2]->push_back(timeChip);
      outputTree[3]->push_back(row/2);
      outputTree[4]->push_back(GetCorrectCol(row, col));
      FindHitsNum++;
    }
    else if (passbreak(target, timeFPGA, 1)) {tag[chipID] = i; break;}
    else if (passcontinue(target, timeFPGA, 1)) {continue;}
  }
  return FindHitsNum;
}
#endif
