#include<iostream>
#include<fstream>
#include<vector>

#include "FindTimeStamp.h"
#include "FindAllHits.h"
#include "CreateCluster.h"
#include "CombineAllTracks.h"


int main(int argc, char **argv)
{
    // ./runReco "/home/dell/MOST2_Offline_Reconstruction/DESY_TbData/" "Run0336" "mhit"
    TString directory = argv[1];
    TString RunNumber = argv[2];
    TString hitOption = argv[3];

    FindTimeStamp* findTimeStamp = new FindTimeStamp();
    findTimeStamp->initialize(directory, RunNumber);
    std::vector<std::vector<int>> Turnpoint = findTimeStamp->FindTurnPoint();
    findTimeStamp->FindTime(Turnpoint, directory, RunNumber);
    delete findTimeStamp;

    FindAllHits* findHits = new FindAllHits();   //这一个需要上一步的Turnpoint作为输入，所以这两个必须同时跑
    findHits->FindHits(Turnpoint, directory, RunNumber);
    delete findHits;
  
    CreateCluster* clustering = new CreateCluster();
    clustering->FormCluster(directory, RunNumber, hitOption);
    delete clustering;

    CombineAllTracks* combinetracks = new CombineAllTracks();
    combinetracks->CombineCluster(directory, RunNumber, hitOption);
    delete combinetracks;



}
