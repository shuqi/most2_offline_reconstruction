## Decode binary file
```
cd scripts
root -l DeBinary.cpp

```
## Track Reconstruction 
### Running 
```
cd build
make
./runReco "/home/dell/MOST2_Offline_Reconstruction/DESY_TbData/" "Run0343" "mhit"

```
### Function of code in src/

*  FindTimeStamp: Find matched FPGA time stamp  
*  FindAllHits: Find all hits with same FPGA time stamp  
*  CreateCluster:  Clustering  
*  CombineAllTracks: Combine all possible tracks  


## Track Fit 
### Running
```
cd TrackFitMinuit  
cd build  
make
./runFit "Run0343" 
```
You can find the output root file in the Output directory



