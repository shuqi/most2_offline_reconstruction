void mplot(){
    TFile *InFile = new TFile("/home/dell/MOST2_Offline_Reconstruction/DESY_TbData/Run0344/AlignedCut1/Clusterxy_all_Eff6.root","read");
    TFile *OutFile = new TFile("/home/dell/MOST2_Offline_Reconstruction/DESY_TbData/Run0344/AlignedCut1/hitmap.root", "RECREATE");
    TTree *tInFile = (TTree*)InFile->Get("TrackInfo");

    std::vector<int>* vec_row      = new std::vector<int>();
    tInFile->SetBranchAddress("row", &vec_row);
    std::vector<int>* vec_col      = new std::vector<int>();
    tInFile->SetBranchAddress("col", &vec_col);
    std::vector<int>* vec_plane      = new std::vector<int>();
    tInFile->SetBranchAddress("planeID", &vec_plane);
    std::vector<int>* vec_hitsNum    = new std::vector<int>();
    tInFile->SetBranchAddress("hitsNum", &vec_hitsNum);


	int evetnum = (int)tInFile->GetEntries();
    TH2D * hitmap[6];
    for(int i = 0; i < 6; i++)
    {
      TString name = "hitmap_plane"+to_string(i);
      hitmap[i] = new TH2D(name, "", 600, 200, 800, 450, 0 , 450);
    }

    for(int m = 0; m < evetnum; m++)
    {
        tInFile->GetEntry(m);
        for (int k = 0; k < vec_plane->size(); k++)
        {
            for (int kk = 0; kk < 6; kk++)
            {
                if (vec_plane->at(k) == kk && vec_hitsNum->at(k) == 2)
                {
                    hitmap[kk]->Fill(vec_col->at(k), vec_row->at(k));
                }     
            }
            
        }
    }
OutFile->Write();

}
