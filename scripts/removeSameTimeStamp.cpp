void remove(TString RunNumber, TString BoardNum){

  TFile *inFile = new TFile("/home/dell/MOST2_Offline_Reconstruction/DESY_TbData/"+RunNumber+"/new2/Outputhit"+BoardNum+"_Original.root", "read");

  TTree *t = (TTree*)inFile->Get("HitsInfo");

  int inchipID, inrow, incol, intimeFPGA, intimeChip;
  double innewrow, innewcol;

  t->SetBranchAddress("chipID", &inchipID);
  t->SetBranchAddress("row", &inrow);
  t->SetBranchAddress("col", &incol);
  t->SetBranchAddress("newrow", &innewrow);
  t->SetBranchAddress("newcol", &innewcol);
  t->SetBranchAddress("timeFPGA", &intimeFPGA);
  t->SetBranchAddress("timeChip", &intimeChip);

  TFile *outFile = new TFile("/home/dell/MOST2_Offline_Reconstruction/DESY_TbData/"+RunNumber+"/new2/Outputhit"+BoardNum+".root", "RECREATE");
  TTree* myTree = new TTree("HitsInfo", "");
  int chipID, row, col, timeFPGA, timeChip;
  myTree->Branch("chipID", &chipID);
  myTree->Branch("row", &row);
  myTree->Branch("col", &col);
  myTree->Branch("timeFPGA", &timeFPGA);
  myTree->Branch("timeChip", &timeChip);
  
  int prerow = -1;
  int precol = -1;
  int pretimeFPGA = -1;
  int pretimeChip = -1;
  bool ifrepeat;
  int tag1 = 0;
  for (int i = 0; i < (int)t->GetEntries(); i++)
  {
    t->GetEntry(i);
    //if (i > 50000) break;
    if (i%1000000 == 0) {std::cout << "i = " << i << std::endl;}
    if (intimeFPGA == 0) {
      std::cout << "i = " << i << std::endl;
      std::cout << "intimeFPGA == 0" << std::endl;
      continue;
    }
    if (intimeFPGA > 80000000) {
      continue;
    }
    if (tag1 == 1)
    {
      pretimeFPGA = timeFPGA;
      pretimeChip = timeChip;
      precol = col;
      prerow = row;
    }
    ifrepeat = (prerow == inrow && precol == incol && pretimeFPGA == intimeFPGA && pretimeChip == intimeChip);

    chipID = inchipID;
    row = inrow;
    col = incol;
    timeFPGA = intimeFPGA;
    timeChip = intimeChip;
    if (ifrepeat) {
      std::cout << "ifrepeat, i = " << i << std::endl; 
      continue;
    }
    int diff1 = pretimeFPGA - intimeFPGA; 
    if ((pretimeFPGA > intimeFPGA)) 
    {
      tag1 = 0;
      
      std::cout << "i = " << i << std::endl;
      std::cout << "pretimeFPGA > intimeFPGA" << std::endl;
      std::cout << "pretimeFPGA = " << pretimeFPGA << std::endl;
      std::cout << "intimeFPGA = " << intimeFPGA << std::endl;
      std::cout << "diff = " << pretimeFPGA - intimeFPGA << std::endl;
      
    }
    else
    {
      if (innewcol >= 250 && innewcol <= 750 && innewrow >= 50 && innewrow <= 425) myTree->Fill();
      tag1 = 1;
    }

    /*
    if (pretimeFPGA > intimeFPGA)
    {
      std::cout << "i = " << i << std::endl;
      std::cout << "pretimeFPGA > intimeFPGA" << std::endl;
      std::cout << "pretimeFPGA = " << pretimeFPGA << std::endl;
      std::cout << "intimeFPGA = " << intimeFPGA << std::endl;
      std::cout << "diff = " << pretimeFPGA - intimeFPGA << std::endl;
      continue;
    }
    */
  }

  myTree->Write();
  inFile->Close();
  outFile->Close();
}

void removeSameTimeStamp(){
  TString RunNumber = "Run0343";
  TString BoardNum;
  for (int i = 1; i < 7; i++){
    BoardNum = "0"+std::to_string(i);
    std::cout << "BoardNum = " << BoardNum << std::endl;
    remove(RunNumber, BoardNum);
  }
}
