bool passTime(int Time1, int Time2)
{
  if (std::abs(Time1 - Time2) <= 2) return true;
  return false;
}

bool passbreak(int Time1, int Time2)
{
  return ((std::abs(Time1 - Time2) > 2) && (Time2 > Time1));
}

bool passcontinue(int Time1, int Time2)
{
  return ((std::abs(Time1 - Time2) > 2) && (Time2 < Time1));
}

int findItems(std::vector<int>* v, int target)
{
  int indices;
  for (auto it = v->begin(); it != v->end(); it++) {
    if (*it == target) {
      indices = std::distance(v->begin(), it);
      return indices;
    }
  }
  return -1;
}

vector<int> FindZero(TFile *inputFile)
{
  vector<int> vec_Findpos;
  TTree *t = (TTree*)inputFile->Get("HitsInfo");
  int timeFPGA;
  t->SetBranchAddress("timeFPGA", &timeFPGA);
  int pretimeFPGA = 0;
  int curtimeFPGA = 0;
  std::cout << "aaaaa" << std::endl;
  int findTimes = 0;
  vec_Findpos.clear();
  for (int i = 0; i < (int)t->GetEntries(); i++)
  {
    t->GetEntry(i);
    pretimeFPGA = curtimeFPGA;
    curtimeFPGA = timeFPGA;
    // std::cout << "pretimeFPGA = " << pretimeFPGA << std::endl;
    // std::cout << "curtimeFPGA = " << curtimeFPGA << std::endl;
    if (pretimeFPGA > curtimeFPGA)
    {
      std::cout << "pretimeFPGA > curtimeFPGA" << std::endl;
      std::cout << i << std::endl;
      std::cout << "pretimeFPGA = " << pretimeFPGA << std::endl;
      std::cout << "curtimeFPGA = " << curtimeFPGA << std::endl;
      std::cout << "==========" << std::endl;
      vec_Findpos.push_back(i);
      findTimes++;
    }
  }
  std::cout << "findTimes = " << findTimes << std::endl;
  return vec_Findpos;
}

int GetCorrectCol(int row, int col)
{
  int newCol;
  if ((row%4 == 0) || (row%4 == 3)){ newCol = col*2; }
  else { newCol = col*2+1; }
  return newCol;
}

int FindTime(TFile *inputFile, int target, vector<vector<int>*> outputTree, int chipID, int tag[5]){
  TTree *t = (TTree*)inputFile->Get("HitsInfo");
  int row, col, timeFPGA, timeChip;
  t->SetBranchAddress("row", &row);
  t->SetBranchAddress("col", &col);
  t->SetBranchAddress("timeFPGA", &timeFPGA);
  t->SetBranchAddress("timeChip", &timeChip);
  // std::cout << (int)t->GetEntries() << std::endl;
  // std::cout << "chipID = " << chipID << std::endl; std::cout << "tag = " << tag[chipID] << std::endl; std::cout << "=============" << std::endl;
  // std::cout << "chipID = " << chipID << std::endl;
  int FindHitsNum = 0;
  // for (int i = 0; i < (int)t->GetEntries(); i++)
  for (int tt = tag[chipID]; tt < (int)t->GetEntries(); tt++)
  {
    t->GetEntry(tt);
   
    if (passTime(target, timeFPGA))
    {
      tag[chipID] = tt;
      outputTree[0]->push_back(chipID);
      outputTree[1]->push_back(timeFPGA);
      outputTree[2]->push_back(timeChip);
      outputTree[3]->push_back(row/2);
      outputTree[4]->push_back(GetCorrectCol(row, col));
      FindHitsNum++;
      // std::cout << "ccccccc" << std::endl;
    }
    else if (passbreak(target, timeFPGA)) { tag[chipID] = tt;  break; std::cout << "aaaaaa" << std::endl;}
    else if (passcontinue(target, timeFPGA)) { continue; std::cout << "bbbbbb" << std::endl;}
    else {std::cout << "else..." << std::endl;}
  }

  std::cout << "aaaksjkdlsdkfldkfldkfl" << std::endl;
  return FindHitsNum;
}

// Get the same FPGAtimeStamp
void FindNew4(){
    TString RunNumber = "Run0659";
    TFile *file1 = new TFile("MOST2_BSRF_Data/"+RunNumber+"/Outputhit05.root", "read");    // last last plane
    TFile *file2 = new TFile("MOST2_BSRF_Data/"+RunNumber+"/Outputhit04.root", "read");    // last plane
    TFile *file3 = new TFile("MOST2_BSRF_Data/"+RunNumber+"/Outputhit03.root", "read");    // second plane
    TFile *file4 = new TFile("MOST2_BSRF_Data/"+RunNumber+"/Outputhit02.root", "read");    // first plane
    TFile *file5 = new TFile("MOST2_BSRF_Data/"+RunNumber+"/Outputhit01.root", "read");    // first plane

    vector<TFile*> AllInputFiles;
    AllInputFiles.push_back(file5);
    AllInputFiles.push_back(file4);
    AllInputFiles.push_back(file3);
    AllInputFiles.push_back(file2);
    AllInputFiles.push_back(file1);
    
    vector<int> vec_Findpos5 = FindZero(file5);
    vector<int> vec_Findpos4 = FindZero(file4);
    vector<int> vec_Findpos3 = FindZero(file3);
    vector<int> vec_Findpos2 = FindZero(file2);
    vector<int> vec_Findpos1 = FindZero(file1);

    std::cout << vec_Findpos5.size() << std::endl;
    std::cout << vec_Findpos4.size() << std::endl;
    std::cout << vec_Findpos3.size() << std::endl;
    std::cout << vec_Findpos2.size() << std::endl;
    std::cout << vec_Findpos1.size() << std::endl;

    for (int j = 0; j < 9; j++)
    {
      std::cout << vec_Findpos1[j] << std::endl;
      std::cout << vec_Findpos2[j] << std::endl;
      std::cout << vec_Findpos3[j] << std::endl;
      std::cout << vec_Findpos4[j] << std::endl;
      std::cout << vec_Findpos5[j] << std::endl;
      std::cout << "=======" << std::endl;
    }

    TFile *OutFile = new TFile("MOST2_BSRF_Data/"+RunNumber+"/ALllSameTimeLooseFirst4Testt.root", "RECREATE");
    TTree* myTree = new TTree("TimeInfo", "beam data");
    // vector<int>* vec_posOut      = new std::vector<int>();
    // myTree->Branch("pointor", &vec_posOut);
    vector<int>* vec_timeFPGAOut = new std::vector<int>();
    myTree->Branch("timeFPGA", &vec_timeFPGAOut);
    vector<int>* vec_timeChipOut    = new std::vector<int>();
    myTree->Branch("timeChip", &vec_timeChipOut);
    vector<int>* vec_rowOut      = new std::vector<int>();
    myTree->Branch("row", &vec_rowOut);
    vector<int>* vec_colOut      = new std::vector<int>();
    myTree->Branch("col", &vec_colOut);
    vector<int>* vec_planeOut    = new std::vector<int>();
    myTree->Branch("planeID", &vec_planeOut);

    vector<vector<int>*> vec_BranchOut;

    TTree *t1 = (TTree*)file1->Get("HitsInfo");
    TTree *t2 = (TTree*)file2->Get("HitsInfo");
    TTree *t3 = (TTree*)file3->Get("HitsInfo");
    TTree *t4 = (TTree*)file4->Get("HitsInfo");

    int row1, row2, row3, row4, col1, col2, col3, col4, timeFPGA1, timeFPGA2, timeFPGA3, timeFPGA4, timeChip1, timeChip2, timeChip3, timeChip4;

    t1->SetBranchAddress("row", &row1);
    t1->SetBranchAddress("col", &col1);
    t1->SetBranchAddress("timeFPGA", &timeFPGA1);
    t1->SetBranchAddress("timeChip", &timeChip1);

    t2->SetBranchAddress("row", &row2);
    t2->SetBranchAddress("col", &col2);
    t2->SetBranchAddress("timeFPGA", &timeFPGA2);
    t2->SetBranchAddress("timeChip", &timeChip2);

    t3->SetBranchAddress("row", &row3);
    t3->SetBranchAddress("col", &col3);
    t3->SetBranchAddress("timeFPGA", &timeFPGA3);
    t3->SetBranchAddress("timeChip", &timeChip3);

    t4->SetBranchAddress("row", &row4);
    t4->SetBranchAddress("col", &col4);
    t4->SetBranchAddress("timeFPGA", &timeFPGA4);
    t4->SetBranchAddress("timeChip", &timeChip4);

    int TotHitsNum4 = (int)t4->GetEntries();
    int TotHitsNum3 = (int)t3->GetEntries();
    int TotHitsNum2 = (int)t2->GetEntries();
    int TotHitsNum1 = (int)t1->GetEntries();

    std::cout << "TotHitsNum4 = " << TotHitsNum4 << std::endl;
    std::cout << "TotHitsNum3 = " << TotHitsNum3 << std::endl;
    std::cout << "TotHitsNum1 = " << TotHitsNum1 << std::endl;
    std::cout << "TotHitsNum2 = " << TotHitsNum2 << std::endl;

    int ltag = 0;
    int ktag = 0;
    int jtag = 0;
    
    int preFPGA = 0;
    int curFPGA = 0;
    int tag[5] = {0};
    for (int i = 0; i < (int)t1->GetEntries(); i++)
    {
      t1->GetEntry(i);
      if (i > 13130) std::cout << "i = " << i << std::endl;
      int Items = findItems(&vec_Findpos1, i);
      if ( Items >= 0)
      {
        std::cout << "found...." << std::endl;
        jtag = vec_Findpos2[Items]; 
        ktag = vec_Findpos3[Items]; 
        ltag = vec_Findpos4[Items]; 
        std::cout << "aaa" << std::endl;
      }
      if (i > 13136) break;
      for (int j = jtag; j < (int)t2->GetEntries(); j++)
      {
        t2->GetEntry(j);
        if (passTime(timeFPGA1, timeFPGA2))
        {
          jtag = j + 1;
          //std::cout << "passTime(timeFPGA1, timeFPGA2)" << std::endl;
          for (int k = ktag; k < (int)t3->GetEntries(); k++)
          {
            //std::cout << "timeFPGA3 = " << timeFPGA3 << std::endl;
            t3->GetEntry(k);
            if (passTime(timeFPGA2, timeFPGA3))
            {
              //std::cout << "passTime(timeFPGA3, timeFPGA2)" << std::endl;
              ktag = k + 1;
              int SavetimeFPGA1Times = 0;
              for (int l = ltag; l < (int)t4->GetEntries(); l++)
              {
                t4->GetEntry(l);
                // std::cout << "l = " << l << std::endl;
                if (passTime(timeFPGA3, timeFPGA4))
                {
                  ltag = l + 1;
                  vec_timeFPGAOut->clear();
                  vec_timeChipOut->clear();
                  vec_rowOut->clear();
                  vec_colOut->clear();
                  vec_planeOut->clear();

                  vec_BranchOut.clear();
                  vec_BranchOut.push_back(vec_planeOut);
                  vec_BranchOut.push_back(vec_timeFPGAOut);
                  vec_BranchOut.push_back(vec_timeChipOut);
                  vec_BranchOut.push_back(vec_rowOut);
                  vec_BranchOut.push_back(vec_colOut);
                  std::cout << "l = " << l << std::endl;
                  std::cout << "passTime(timeFPGA3, timeFPGA4)" << std::endl;
                  if (SavetimeFPGA1Times == 0)
                  {
                  // if (SavetimeFPGA1Times == 0)
                  // {
                    preFPGA = curFPGA;
                    curFPGA = timeFPGA1;
                    if (preFPGA > curFPGA){
                      std::cout << "i = " << i << std::endl;
                      std::cout << "j = " << j << std::endl;
                      std::cout << "k = " << k << std::endl;
                      std::cout << "preFPGA = " << preFPGA << std::endl;
                      std::cout << "timeFPGA1 = " << timeFPGA1 << std::endl;
                    }
                 
                    int FindTotalHits[5] = {0};
                    for (int ff = 0; ff < 5; ff++)
                    {
                      FindTotalHits[ff] = FindTime(AllInputFiles[ff], timeFPGA1, vec_BranchOut, ff, tag);
                    }
                    std::cout << " FindTotalHits[0] = " << FindTotalHits[0] << std::endl;
                    std::cout << " FindTotalHits[1] = " << FindTotalHits[1] << std::endl;
                    std::cout << " FindTotalHits[2] = " << FindTotalHits[2] << std::endl;
                    std::cout << " FindTotalHits[3] = " << FindTotalHits[3] << std::endl;
                    std::cout << " FindTotalHits[4] = " << FindTotalHits[4] << std::endl;
                    if (FindTotalHits[3] > 0 && FindTotalHits[2] > 0 && FindTotalHits[1] > 0 && FindTotalHits[0] > 0)
                    {
                      // myTree->Fill();
                      std::cout << "aaaijdidji" << std::endl;
                    }
                    else
                    {
                      std::cout << i << "no hit in one chip !!!!!" << std::endl;
                    }
                    std::cout << "SavetimeFPGA1Times...." << std::endl;
                    SavetimeFPGA1Times++;
                  }
                }
                else if (passbreak(timeFPGA3, timeFPGA4)) { ltag = l; break;}
                else if (passcontinue(timeFPGA3, timeFPGA4)) continue;
                std::cout << "endddddd...." << std::endl;
              }
            }
            else if (passbreak(timeFPGA2, timeFPGA3)) { ktag = k; break;}
            else if (passcontinue(timeFPGA2, timeFPGA3)) { continue; }
          }
        }
        else if (passbreak(timeFPGA1, timeFPGA2)) { jtag = j; break;}
        else if (passcontinue(timeFPGA1, timeFPGA2)) continue;
      }
    }
  // myTree->Write();
}
