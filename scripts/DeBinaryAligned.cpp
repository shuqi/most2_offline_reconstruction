#include<iostream>
#include<fstream>
#include<vector>
#include "Math/Rotation3D.h"
#include "Math/Transform3D.h"
using namespace std;

double setAlignment[6];

void pixelToPoint(int row, int col, double Point[2])
{
  if (col <= 1024/2)
  {
    Point[0] = -((511 - col)*25 + 25/2);
  }
  else
  {
    Point[0] = (col - 512)*25 + 25/2;
  }

  if (row  <= 512/2)
  {
    Point[1] = -((255 - row)*25 + 25/2);
  }
  else
  {
    Point[1] = (row - 256)*25 + 25/2;
  }
}

ROOT::Math::Transform3D readConditions(const std::string& filename, int i)
{
  std::ifstream fin(filename);
  std::string line_info;
  double input_result;
  std::vector<double> vectorString;
  if (fin)
  {
    while (getline (fin, line_info))
    {
      std::stringstream input(line_info);
      while(input>>input_result)
      {
        vectorString.push_back(input_result);
      }
    }
  }
  else
  {
    std::cout<<"no such file"<<std::endl;
  }

  for (int k = 1; k < 7; k++)
  {
    std::cout << "vectorString[i*7+1] = " << vectorString[i*7+k]  << std::endl;
    setAlignment[k-1] = vectorString[i*7+k];
  }
  ROOT::Math::Translation3D t(vectorString[i*7+1], vectorString[i*7+2], vectorString[i*7+3]);
  ROOT::Math::RotationZYX r(vectorString[i*7+4], vectorString[i*7+5], vectorString[i*7+6]);
  ROOT::Math::Transform3D m_transform = ROOT::Math::Transform3D(r, t);

  ROOT::Math::XYZPoint check;
  m_transform.GetTranslation(check);
  std::cout << check.x() << std::endl;
  std::cout << check.y() << std::endl;
  std::cout << check.z() << std::endl;

  fin.close();
  return m_transform;
}

ROOT::Math::XYZPoint localToGlobal(const ROOT::Math::XYZPoint& p, ROOT::Math::Transform3D m_transform)
{
  return m_transform * p;
}


void DoReading(TString RunNumber, TString Chip, ROOT::Math::Transform3D m_transform)
{
  TString directory = "/home/dell/MOST2_Offline_Reconstruction/DESY_TbData/";
  TFile* OutputFile = new TFile(directory+RunNumber+"/Aligned/Outputhit"+Chip+".root", "RECREATE");
  TTree* myTree = new TTree("HitsInfo", "test beam data");

  int valid     = -1;
  int begFlag   = -1;
  int chipID    = 0;
  int dataCount = 0;
  int curID     = 0;

  int preID     = 0;
  int passVal   = 0;
  int row       = 0;
  double newrow = 0;
  double newcol = 0;
  double xglo;
  double yglo;
  double xloc;
  double yloc;
  double zglo;
  int col       = 0;
  int timeFPGA  = 0;
  int timeChip  = 0;

  int prerow       = 0;
  int precol       = 0;
  int pretimeFPGA  = 0;
  int pretimeChip  = 0;

  int hitNumber = 0;  // in every packet
  bool IsTrailer = false;
  int num = 0;

  myTree->Branch("PacketID", &curID);
  myTree->Branch("PacketDataNum", &dataCount);
  myTree->Branch("HitsNum", &hitNumber);

  myTree->Branch("valid", &valid);
  myTree->Branch("chipID", &chipID);
  myTree->Branch("timeFPGA", &timeFPGA);
  myTree->Branch("timeChip", &timeChip);
  myTree->Branch("row", &row);
  myTree->Branch("col", &col);
  myTree->Branch("xloc", &xloc);
  myTree->Branch("yloc", &yloc);
  myTree->Branch("xglo", &xglo);
  myTree->Branch("yglo", &yglo);
  myTree->Branch("zglo", &zglo);

  size_t length;
  ifstream inF;
  
  inF.open(directory+RunNumber+"/"+RunNumber+"-RunData-Board"+Chip+"/"+"Board-"+Chip+"(0000).bin", std::ifstream::binary);
  inF.seekg(0, ios::end);
  length = inF.tellg();
  cout << "the length of the file is " << length << " " << "byte" << endl;
  
  length = 8;
  unsigned char* data = new unsigned char[length]();

  inF.clear();
  inF.seekg(7, ios::beg);
  cout << "the position of the beginner " << inF.tellg() << endl;
  inF.get();
  if (!inF.eof()) {
    cout << "target reading..." << endl;
    while(inF.read(reinterpret_cast<char*>(data), sizeof(char)* length))
    {
      num++;
      if (num%500000 == 0) std::cout << "processing " << num << " packet..." << std::endl; 
      unsigned long long int* data0 = new unsigned long long int[length];
      // for (size_t i = 0; i < length; i++){
      //   data0[i] = data[i];
      //   cout << setw(2) << setfill('0') << setiosflags(ios::uppercase) << hex << data0[i]<<" ";
      // }
      // cout << "" << endl;
      // cout << "the position of the current pointor " << dec <<inF.tellg() << endl;
      if (data[0] == 0xaa && data[1] == 0xbb){
        hitNumber = 0;
        // cout << dec << "Reading one packet..." << endl;
        dataCount = data[2] & 0xff;
        preID = curID;
        curID = (data[4] << 24 & 0xffffffff) | (data[5] << 16 & 0xffffff) | (data[6] << 8 & 0xffff) | (data[7] & 0xff);
        int testID = preID + 1;
        if (curID - preID != 1) cout << "packet header ID error, Current ID should be " << dec << testID << ", rather than " << curID << endl;
        while (inF.read(reinterpret_cast<char*>(data), sizeof(char)* length))
        {
          precol = col;
          prerow = row;
          pretimeChip = timeChip;
          pretimeFPGA = timeFPGA;

          valid  = data[7] & 0x01;
          chipID = data[0] >> 4 & 0x0f;
          timeFPGA = (data[1] << 24 & 0xfffffff) | (data[2] << 16 & 0xffffff) | (data[3] << 8 & 0xffff) | (data[4] & 0xff);
          timeChip = (data[0] << 4 & 0xf0) | (data[1] >> 4 & 0x0f);
          row      = (data[6] << 3 & 0x3ff) | (data[7] >>5 & 0x07);
          col      = (data[5] << 1 & 0x1ff) | (data[6] >> 7 & 0x01);
          if (precol == col && prerow == row && pretimeChip == timeChip && pretimeFPGA == timeFPGA) continue;

          newrow = row/2;
          if ((row%4 == 0) || (row%4 == 3)) { newcol = col*2;}
          else {newcol = col*2 + 1;}
	  double *PointXY = new double[2];
	  pixelToPoint(newrow, newcol, PointXY);
	  xloc = PointXY[0]*(1e-3);
	  yloc = PointXY[1]*(1e-3);

          auto pGlobal = m_transform * ROOT::Math::XYZPoint(xloc, yloc, 0.);

	  xglo = pGlobal.x();
	  yglo = pGlobal.y();
	  zglo = pGlobal.z();

          if ( data[0] == 0xbb && data[1] == 0xaa ) break;
          if ( newcol < 250 || newcol > 750 || newrow < 50 || newrow > 425 ) continue;  // PCB window
          // if ( newcol < 650 || newcol > 750 || newrow < 50 || newrow > 425 ) continue;  // PCB
          hitNumber ++;
          myTree->Fill();
        }
      }
      else if ( data[0] == 0xbb && data[1] == 0xaa ) std::cout << "No packet header, trailer only ..." << std::endl;
    }
  }
  else { std::cout << "No found input files..." << std::endl; }
  inF.close();

  myTree->Write();
  
  OutputFile->Close();
  delete[] data;
}


void DeBinaryAligned()
{
  TString RunNumber = "Run0400";
  int NumOfChips = 6;
  const std::string& filename = "/home/dell/MOST2_Offline_Align/build/OutputFiles/Run0400/Alignment_out_Run0400_mhit.dat";

  // ROOT::Math::Transform3D m_transform = readConditions(filename, 0);
  for (int i = 1; i < 2; i++)
  {
    TString Chip = "0" + std::to_string(i);
    std::cout << "Transfer local position to global position using alignment parameters" << std::endl; 
    ROOT::Math::Transform3D m_transform = readConditions(filename, i-1);
    std::cout << "======== Decoding " << Chip << "th " << "chip file =======" << std::endl;
    DoReading(RunNumber, Chip, m_transform);
    std::cout << "================= Done =================" << std::endl;   
  }    
}
