#include <TFile.h>

#include "IncludeFun.h"
#include "island1.h"
#include "constants.h"
#include "FindAllHits.h"

// Get all hits with the same FPGA timeStamp

FindAllHits::FindAllHits()
{
  std::cout << "********************************************" << std::endl;
  std::cout << "*** finding all hits with same FPGA time ***"  << std::endl;
  std::cout << "********************************************" << std::endl;
}

FindAllHits::~FindAllHits()
{

}

//这个代码开始把上一布找到的时间戳在各个芯片上寻找对应的所有hits组成一个事例。
void FindAllHits::FindHits(std::vector<std::vector<int>> vec_Findpos, TString directory, TString RunNumber){

    TFile *Orifile1 = new TFile(directory+RunNumber+"/new/Outputhit01.root", "read"); // first plane   
    TFile *Orifile2 = new TFile(directory+RunNumber+"/new/Outputhit02.root", "read");    
    TFile *Orifile3 = new TFile(directory+RunNumber+"/new/Outputhit03.root", "read");    
    TFile *Orifile4 = new TFile(directory+RunNumber+"/new/Outputhit04.root", "read");    
    TFile *Orifile5 = new TFile(directory+RunNumber+"/new/Outputhit05.root", "read");
    TFile *Orifile6 = new TFile(directory+RunNumber+"/new/Outputhit06.root", "read"); 


    std::vector<TFile*> AllInputFiles;
    AllInputFiles.push_back(Orifile1);
    AllInputFiles.push_back(Orifile2);
    AllInputFiles.push_back(Orifile3);
    AllInputFiles.push_back(Orifile4);
    AllInputFiles.push_back(Orifile5);
    AllInputFiles.push_back(Orifile6);

    TFile *InFile = new TFile(directory+RunNumber+"/newTimeChip/SameTimeLooseFirst4Board.root", "read"); //这是上一步找到的符合的时间戳是多少
    TTree *tInFile = (TTree*)InFile->Get("TimeInfo");

    TFile *OutFile = new TFile(directory+RunNumber+"/newTimeChip/AllSameTimeHitsLoose.root", "RECREATE");

    // Out Tree
    TTree* myTree = new TTree("TimeInfo", "beam data");
    int EventNum = 0;
    myTree->Branch("EventNum", &EventNum);
    vector<int>* vec_timeFPGAOut = new std::vector<int>();
    myTree->Branch("timeFPGA", &vec_timeFPGAOut);

    vector<int>* vec_timeChipOut = new std::vector<int>();
    myTree->Branch("timeChip", &vec_timeChipOut);
    
    int timeNum = 0;
    myTree->Branch("timeNum", &timeNum);

    vector<int>* vec_timeChipIDOut = new std::vector<int>();
    myTree->Branch("timeChipID", &vec_timeChipIDOut);

    vector<int>* vec_planeOut    = new std::vector<int>();
    myTree->Branch("planeID", &vec_planeOut);

    vector<int>* vec_rowOut      = new std::vector<int>();
    myTree->Branch("row", &vec_rowOut);

    vector<int>* vec_colOut      = new std::vector<int>();
    myTree->Branch("col", &vec_colOut);

    vector<int>* vec_clusterOut    = new std::vector<int>();
    myTree->Branch("clusterID", &vec_clusterOut);

    vector<int>* vec_clusterNum    = new std::vector<int>();
    myTree->Branch("clusterNum", &vec_clusterNum);

    int TotalClusters = 0;
    myTree->Branch("TotalClusters", &TotalClusters);

    vector<vector<int>*> vec_BranchOut;

    vector<int>* vec_pos = new std::vector<int>();
    tInFile->SetBranchAddress("pointor", &vec_pos);
    vector<int>* vec_timeFPGA = new std::vector<int>();
    tInFile->SetBranchAddress("timeFPGA", &vec_timeFPGA);
    vector<int>* vec_timeChip = new std::vector<int>();
    tInFile->SetBranchAddress("timeChip", &vec_timeChip);
    vector<int>* vec_row      = new std::vector<int>();
    tInFile->SetBranchAddress("row", &vec_row);
    vector<int>* vec_col      = new std::vector<int>();
    tInFile->SetBranchAddress("col", &vec_col);
    vector<int>* vec_plane      = new std::vector<int>();
    tInFile->SetBranchAddress("planeID", &vec_plane);

    int preFPGA = 0;
    int curFPGA = 0;
    int tag[NumOfChips]; for (int i = 0; i < NumOfChips; i++) tag[i] = 0;
    int ith = 0;

    int TotalHits;
    vector<double> Selrow; vector<double> Selcol; vector<int> SelTimeChip; vector<int> SelTimeChipID;
    for (int m = 0; m < (int)tInFile->GetEntries(); m++)
    {
      tInFile->GetEntry(m);
      int target = vec_timeFPGA->at(0);
      preFPGA = curFPGA;
      curFPGA = target;
      if (preFPGA > curFPGA){   //如果后面的时间戳大于前面的时间戳，意味着进入一个新的时间戳循环
        std::cout << "ith = " << ith << std::endl;
        std::cout << "preFPGA = " << preFPGA << std::endl;
        std::cout << "timeFPGA1 = " << target << std::endl;

        tag[0] = vec_Findpos[0][ith];   //tag的作用是在监测到有新循环时，让芯片循环事例的位置直接从循环出现的位置处继续循环。
        tag[1] = vec_Findpos[1][ith];
        tag[2] = vec_Findpos[2][ith];
        tag[3] = vec_Findpos[3][ith];
        tag[4] = vec_Findpos[4][ith];
        tag[5] = vec_Findpos[5][ith];
        std::cout << "tag[0] = " << tag[0] << std::endl;
        std::cout << "tag[1] = " << tag[1] << std::endl;
        std::cout << "tag[2] = " << tag[2] << std::endl;
        std::cout << "tag[3] = " << tag[3] << std::endl;
        std::cout << "tag[4] = " << tag[4] << std::endl;
        std::cout << "tag[5] = " << tag[5] << std::endl;
        ith++;
        std::cout << "****************" << std::endl;
      }
      

      vec_timeFPGAOut->clear();
      vec_timeChipOut->clear();
      vec_rowOut->clear();
      vec_colOut->clear();
      vec_planeOut->clear();
      vec_clusterOut->clear();
      vec_clusterNum->clear();
      vec_timeChipIDOut->clear();

      vec_BranchOut.clear();

      vec_BranchOut.push_back(vec_planeOut);
      vec_BranchOut.push_back(vec_timeFPGAOut);
      vec_BranchOut.push_back(vec_timeChipOut);
      vec_BranchOut.push_back(vec_rowOut);
      vec_BranchOut.push_back(vec_colOut);

      if (m%100000 == 0) std::cout << "processing " << m << " events" <<std::endl;
      // std::cout << "m = " << m << std::endl;
      int FindTotalHits[NumOfChips]; for (int i = 0; i < NumOfChips; i++) FindTotalHits[i] = 0;
      for (int i = 0; i < NumOfChips; i++)
      {
        FindTotalHits[i] = FindTime(AllInputFiles[i], target, vec_BranchOut, i, tag);  //tag是每个芯片循环事例时从哪个位置开始，target是要找的hits的时间戳是多少
      }
      //如果在所有的6个芯片上都找到了，开始把同一个芯片上的hit根据位置进行聚类，并把归为一个cluster的hit标上同样的序号。
      TotalClusters = 0;
      if (FindTotalHits[0] > 0 && FindTotalHits[1] > 0 && FindTotalHits[2] > 0 && FindTotalHits[3] > 0 && FindTotalHits[4] > 0 && FindTotalHits[5] > 0)
      {
        EventNum++;
        TotalHits = vec_timeFPGAOut->size();
        
        int aa[TotalHits]; for (int i = 0; i < TotalHits; i++) aa[i] = 0;
        int accuPlaneHits = 0;
        timeNum = 0;
        for (int i = 0; i < NumOfChips; i++)
        {
          Selrow.clear();
          Selcol.clear();
          SelTimeChip.clear();
          SelTimeChipID.clear();
          
          std::vector<int> Selplane = findVectors(vec_planeOut, i);  //把第i个芯片所有的hit在整个事例的vector中的位置记录下来
          for (int j = Selplane[0]; j < Selplane[Selplane.size() - 1] + 1; j++)
          {
            Selrow.push_back(vec_rowOut->at(j));  //把在这个芯片上所有的hit的像素位置都放到一个vector中。
            Selcol.push_back(vec_colOut->at(j));
            SelTimeChip.push_back(vec_timeChipOut->at(j));
          }

          SelTimeChipID = FindTimeChip(SelTimeChip);
          // std::cout << "SelTimeChipID.size() = " << SelTimeChipID.size() << std::endl;
          for (int kk = 0; kk < SelTimeChipID.size(); kk++)
          {
            // std::cout << "SelTimeChipID.at(kk) = " << SelTimeChipID.at(kk) << std::endl;
            vec_timeChipIDOut->push_back(SelTimeChipID.at(kk));
          }
          timeNum += *max_element(SelTimeChipID.begin(), SelTimeChipID.end()) + 1;

          // std::cout << "===" << std::endl;

          double size = 0.025;
          for(auto& tx:Selrow){ tx*= size; }
          for(auto& ty:Selcol){ ty*= size; }

          island *t_island = new island(Selrow, Selcol);
          unordered_map<int,vector<pair<double, double>>> islands = t_island->getislands();
          unordered_map<int,vector<int>> islands_id = t_island->getislands_id();

          for(int i = 0; i<t_island->getnumOfislands();i++){
            //cout<<"cluster "<<i<<endl;
            for(int j=0;j<islands[i].size();j++){
              //cout<<"(x,y):"<<int(islands[i][j].first*40+EPSINON)<<","<<int(islands[i][j].second*40+EPSINON)<<" id:"<<islands_id[i][j]<<endl;
              int index = islands_id[i][j];
              //std::cout<<"islands_id[i][j] = " << index << std::endl; //
              //std::cout << "i = " << i << std::endl;
              aa[accuPlaneHits+index] = i;
              vec_clusterNum->push_back(t_island->getnumOfislands());
            }
            // cout<<endl;
          }
          //std::cout << "==================" << std::endl;
          TotalClusters += t_island->getnumOfislands();
          delete t_island;
          //std::cout << "Selrow.size() = " << Selrow.size() << std::endl;
          accuPlaneHits = accuPlaneHits + Selrow.size();
          
        }
        for (int i = 0; i < TotalHits; i++)
        {
          //std::cout << "aa[i] = " << aa[i] << std::endl;
          vec_clusterOut->push_back(aa[i]);
          // std::cout << vec_timeChipOut->at(i) << std::endl; 
        }  

        // std::cout << "TotalClusters = " << TotalClusters << std::endl;
        myTree->Fill();
        // if (EventNum == 6) break;
      }
    }
  myTree->Write();
  InFile->Close();
  OutFile->Close();
}