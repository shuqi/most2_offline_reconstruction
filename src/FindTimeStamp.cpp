#include "IncludeFun.h"
#include "constants.h"
#include "FindTimeStamp.h"

#include <iostream>
#include <fstream>
#include <vector>

// Get the same FPGAtimeStamp
FindTimeStamp::FindTimeStamp()
{
  std::cout << "**********************************" << std::endl;
  std::cout << "*** finding FPGA time matched ***"  << std::endl;
  std::cout << "**********************************" << std::endl;
}

FindTimeStamp::~FindTimeStamp()
{

}

bool FindTimeStamp::initialize(TString directory, TString RunNumber)
{
  file1 = new TFile(directory+RunNumber+"/new/Outputhit01.root", "read"); 
  file2 = new TFile(directory+RunNumber+"/new/Outputhit02.root", "read");
  file3 = new TFile(directory+RunNumber+"/new/Outputhit03.root", "read");
  file4 = new TFile(directory+RunNumber+"/new/Outputhit04.root", "read");
  file5 = new TFile(directory+RunNumber+"/new/Outputhit05.root", "read");
  file6 = new TFile(directory+RunNumber+"/new/Outputhit06.root", "read");

  return true;
}

std::vector<std::vector<int>> FindTimeStamp::FindTurnPoint()
{
  std::vector<std::vector<int>> vec_Findpos;
  // std::cout << "1" << std::endl;
  std::vector<int> vec_Findpos1 = FindZero(file1);   //找循环的节点所在的位置，后一个数小于前一个数，即循环了一次。
  // std::cout << "2" << std::endl;
  std::vector<int> vec_Findpos2 = FindZero(file2);   //每一个芯片的循环次数都要找出来
  // std::cout << "3" << std::endl;
  std::vector<int> vec_Findpos3 = FindZero(file3);
  // std::cout << "4" << std::endl;
  std::vector<int> vec_Findpos4 = FindZero(file4);
  // std::cout << "5" << std::endl;
  std::vector<int> vec_Findpos5 = FindZero(file5);
  // std::cout << "6" << std::endl;
  std::vector<int> vec_Findpos6 = FindZero(file6);

  std::cout << vec_Findpos1.size() << std::endl;     //输出每一个芯片循环的次数
  std::cout << vec_Findpos2.size() << std::endl;
  std::cout << vec_Findpos3.size() << std::endl;
  std::cout << vec_Findpos4.size() << std::endl;
  std::cout << vec_Findpos5.size() << std::endl;
  std::cout << vec_Findpos6.size() << std::endl;

  std::vector<int> loopNum;
  loopNum.push_back(vec_Findpos1.size());  //把每一个芯片循环的次数填到一个vector中
  loopNum.push_back(vec_Findpos2.size());
  loopNum.push_back(vec_Findpos3.size());
  loopNum.push_back(vec_Findpos4.size());
  loopNum.push_back(vec_Findpos5.size());
  loopNum.push_back(vec_Findpos6.size());

  maxLoop = *max_element(loopNum.begin(), loopNum.end());  //利用此vector来判断各个芯片之间循环的次数是否相同，如果不相同，找出循环次数最大的芯片是哪一个，最小的是哪一个。
  minLoop = *min_element(loopNum.begin(), loopNum.end()); 

  maxPosLoop = max_element(loopNum.begin(),loopNum.end()) - loopNum.begin();
  minPosLoop = min_element(loopNum.begin(),loopNum.end()) - loopNum.begin();

  std::cout << "minLoop = " << minLoop << std::endl;    
  std::cout << "maxLoop = " << maxLoop << std::endl;

  std::cout << "minPosLoop = " << minPosLoop << std::endl;
  std::cout << "maxPosLoop = " << maxPosLoop << std::endl;

  for (int j = 0; j < minLoop; j++)  //输出在每个芯片上找到的循环出现的位置，也就是在第几个hit处有循环。
  {
    std::cout << vec_Findpos1[j] << std::endl;  
    std::cout << vec_Findpos2[j] << std::endl;
    std::cout << vec_Findpos3[j] << std::endl;
    std::cout << vec_Findpos4[j] << std::endl;
    std::cout << vec_Findpos5[j] << std::endl;
    std::cout << vec_Findpos6[j] << std::endl;
    std::cout << "=======" << std::endl;
  }

  vec_Findpos.push_back(vec_Findpos1);  //把每个芯片循环所在位置的vector再填到一个二维vector中。
  vec_Findpos.push_back(vec_Findpos2);
  vec_Findpos.push_back(vec_Findpos3);
  vec_Findpos.push_back(vec_Findpos4);
  vec_Findpos.push_back(vec_Findpos5);
  vec_Findpos.push_back(vec_Findpos6);

  return vec_Findpos;
}

//此代码的目的是找到前4个芯片符合的时间戳是多少，只有最后一步循环的芯片才把有这个时间戳的所有hit都存下来，前面的应该都只保存了一个。
//所以需要下面的代码，根据已经找到的符和时间戳，把每个芯片上具有该时间戳的所有hit都存下来。
void FindTimeStamp::FindTime(std::vector<std::vector<int>> vec_Findpos, TString directory, TString RunNumber){

    TFile *OutFile = new TFile(directory+RunNumber+"/newTimeChip/SameTimeLooseFirst4Board.root", "RECREATE");
    TTree* myTree = new TTree("TimeInfo", "beam data");

    // TTree *t1 = (TTree*)file4->Get("HitsInfo");   // Chip04
    // TTree *t2 = (TTree*)file3->Get("HitsInfo");   // Chip03
    // TTree *t3 = (TTree*)file2->Get("HitsInfo");   // Chip02
    // TTree *t4 = (TTree*)file1->Get("HitsInfo");   // Chip01

    TTree *t1 = (TTree*)file4->Get("HitsInfo");   // Chip05  //先利用前4个芯片，做迭代循环，找出这4个芯片上时间戳符合的
    TTree *t2 = (TTree*)file3->Get("HitsInfo");   // Chip04
    TTree *t3 = (TTree*)file2->Get("HitsInfo");   // Chip03
    TTree *t4 = (TTree*)file1->Get("HitsInfo");   // Chip01   in order to calculate efficiency skipping chip02


    std::vector<int>* vec_pos = new std::vector<int>();
    myTree->Branch("pointor", &vec_pos);
    std::vector<int>* vec_timeFPGA = new std::vector<int>();
    myTree->Branch("timeFPGA", &vec_timeFPGA);
    std::vector<int>* vec_timeChip = new std::vector<int>();
    myTree->Branch("timeChip", &vec_timeChip);
    std::vector<int>* vec_row      = new std::vector<int>();
    myTree->Branch("row", &vec_row);
    std::vector<int>* vec_col      = new std::vector<int>();
    myTree->Branch("col", &vec_col);
    std::vector<int>* vec_plane      = new std::vector<int>();
    myTree->Branch("planeID", &vec_plane);

    int row1, row2, row3, row4, col1, col2, col3, col4, timeFPGA1, timeFPGA2, timeFPGA3, timeFPGA4, timeChip1, timeChip2, timeChip3, timeChip4;

    t1->SetBranchAddress("row", &row1);    //提取4个芯片root文件中的tree
    t1->SetBranchAddress("col", &col1);
    t1->SetBranchAddress("timeFPGA", &timeFPGA1);
    t1->SetBranchAddress("timeChip", &timeChip1);

    t2->SetBranchAddress("row", &row2);
    t2->SetBranchAddress("col", &col2);
    t2->SetBranchAddress("timeFPGA", &timeFPGA2);
    t2->SetBranchAddress("timeChip", &timeChip2);

    t3->SetBranchAddress("row", &row3);
    t3->SetBranchAddress("col", &col3);
    t3->SetBranchAddress("timeFPGA", &timeFPGA3);
    t3->SetBranchAddress("timeChip", &timeChip3);

    t4->SetBranchAddress("row", &row4);
    t4->SetBranchAddress("col", &col4);
    t4->SetBranchAddress("timeFPGA", &timeFPGA4);
    t4->SetBranchAddress("timeChip", &timeChip4);

    int TotHitsNum4 = (int)t4->GetEntries();
    int TotHitsNum3 = (int)t3->GetEntries();
    int TotHitsNum2 = (int)t2->GetEntries();
    int TotHitsNum1 = (int)t1->GetEntries();

    std::cout << "TotHitsNum of chip1 = " << TotHitsNum4 << std::endl;
    std::cout << "TotHitsNum of chip2 = " << TotHitsNum3 << std::endl;
    std::cout << "TotHitsNum of chip3 = " << TotHitsNum2 << std::endl;
    std::cout << "TotHitsNum of chip4 = " << TotHitsNum1 << std::endl;

    int ltag = 0;
    int ktag = 0;
    int jtag = 0;
    
    int preFPGA = 0;
    for (int i = 0; i < (int)t1->GetEntries(); i++)
    {
      t1->GetEntry(i);
      if (i%100000 == 0) std::cout << "i = " << i << std::endl;
      // if (i > 1000000) break;
      int Items = findItems(&vec_Findpos[maxPosLoop], i);   //t1中循环到的i正好在第几个循环的节点处，意味着到了一个循环节点，开始下一轮的大时间戳
      if (Items >= 0 && (minLoop == maxLoop))
      {
        // break;
        std::cout << "found...." << std::endl;      //jag, ktag, ltag是在有新的循环的情况下，让各个芯片全都从各自循环节点处开始循环。
        jtag = vec_Findpos[2][Items];   // 3th chip
        ktag = vec_Findpos[1][Items];   // 2th chip
        ltag = vec_Findpos[0][Items];   // 1th chip
      }
      else if (Items >= 0 && (minLoop != maxLoop))   //如果各个芯片的循环次数不相等，要在达到循环次数时停止循环。
      {
        std::cout << "found...." << std::endl;
        std::cout << "but minLoop != maxLoop" << std::endl;
        if (Items == minLoop)
        {
          break;
        }
        else
        {
          jtag = vec_Findpos[2][Items];   // 3th chip
          ktag = vec_Findpos[1][Items];   // 2th chip
          ltag = vec_Findpos[0][Items];   // 1th chip  
        }
      }
      for (int j = jtag; j < (int)t2->GetEntries(); j++)
      {
        t2->GetEntry(j);
        if (passTime(timeFPGA1, timeFPGA2, 1))  //以timeFPGA1为基础，在2th芯片上找到了时间戳符合的击中，继续向下循环
        {
          jtag = j + 1;
          for (int k = ktag; k < (int)t3->GetEntries(); k++)
          {
            t3->GetEntry(k);
            if (passTime(timeFPGA2, timeFPGA3, 1)) //
            {
              ktag = k + 1;
              vec_timeFPGA->clear();
              vec_timeChip->clear();
              vec_row->clear();
              vec_col->clear();
              vec_plane->clear();
              vec_pos->clear();

              int SavetimeFPGA1Times = 0;
              for (int l = ltag; l < (int)t4->GetEntries(); l++)
              {
                t4->GetEntry(l);
                if (passTime(timeFPGA3, timeFPGA4, 1))
                {
                  ltag = l + 1;
                  // std::cout << "passTime(timeFPGA3, timeFPGA4)" << std::endl;
                  if ( SavetimeFPGA1Times == 0)
                  {
                    vec_timeFPGA->push_back(timeFPGA1);
                    vec_timeChip->push_back(timeChip1);
                    vec_row->push_back(row1);
                    vec_col->push_back(col1);
                    vec_plane->push_back(3);
                    vec_pos->push_back(i);

                    vec_timeFPGA->push_back(timeFPGA2);
                    vec_timeChip->push_back(timeChip2);
                    vec_row->push_back(row2);
                    vec_col->push_back(col2);
                    vec_plane->push_back(2);
                    vec_pos->push_back(j);

                    vec_timeFPGA->push_back(timeFPGA3);
                    vec_timeChip->push_back(timeChip3);
                    vec_row->push_back(row3);
                    vec_col->push_back(col3);
                    vec_plane->push_back(1);
                    vec_pos->push_back(k);
                    SavetimeFPGA1Times++;
                  }

                  vec_timeFPGA->push_back(timeFPGA4);    //在最后一块芯片上找到的与目标时间戳相同的所有的hit, 但是前三块芯片只保存了一个。
                  vec_timeChip->push_back(timeChip4);
                  vec_row->push_back(row4);
                  vec_col->push_back(col4);
                  vec_plane->push_back(0);
                  vec_pos->push_back(l);
                }
                else if (passbreak(timeFPGA3, timeFPGA4, 1)) { ltag = l; break;}
                else if (passcontinue(timeFPGA3, timeFPGA4, 1)) continue;
              }
              if(vec_timeFPGA->size() > 0)  myTree->Fill();
            }
            else if (passbreak(timeFPGA2, timeFPGA3, 1)) { ktag = k; break;}
            else if (passcontinue(timeFPGA2, timeFPGA3, 1)) { continue; }
          }
        }
        else if (passbreak(timeFPGA1, timeFPGA2, 1)) { jtag = j; break;}
        else if (passcontinue(timeFPGA1, timeFPGA2, 1)) continue;
      }
    }
  myTree->Write();
  OutFile->Close();
  
}
